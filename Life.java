import java.util.Hashtable;

/**
 * This is an implementation of Conway's game of Life, with a slight
 * twist in that cells have an "age" reflecting how long a given cell
 * has stayed alive.
 * 
 * @author zeil
 *
 */
public class Life {

    private int theSize;
    private Hashtable<Integer, Integer> cells;
    private int pattern;
    
    
    
    public static final String[] patternNames = {"Box", "R-Pentomino", "Acorn", "Rabbits", "Gliders", "Spaceships", "Puffer Train"}; 
    
    
    private static final String[] acorn_start = {
    		".*.....",
    		"...*...",
    		"**..***"
    }; 
    
    private static final String[] rabbits_start = {
		"*...***",
		"***..*.",
		".*....."
    }; 

    private static final String[] gliders_start = {
    		"***....*.",
    		"*.......*",
    		".*....***"
    }; 

    
    private static final String[] rpentomino_start = {
    		".**",
    		"**.",
    		".*."
    }; 

    
    private static final String[] spaceships_start = {
    		".*..*.....***.",
    		"*.........*..*",
    		"*...*.....*...",
    		"****......*...",
    		"...........*.*"
    }; 
    

    private static final String[] puffertrain_start = {
    		".***......*.....*......***.",
    		"*..*.....***...***.....*..*",
    		"...*....**.*...*.**....*...",
    		"...*...................*...",
    		"...*..*.............*..*...",
    		"...*..**...........**..*...",
    		"..*...**...........**...*.."
    }; 

    private static final String[][] starters = {rpentomino_start, acorn_start, rabbits_start, gliders_start, 
    		spaceships_start, puffertrain_start};   
    
    
    /**
     * Create a new Life state, using a default initial patter of live cells,
     * in a square game of a specified size.
     * 
     * @param initialSize   number of cells in a row/column in this square game.
     */
    public Life(int initialSize)
    {
        setSize (initialSize);
        reset();
        pattern = -1;
    }


    /**
     * Return the number of rows/columns in this Life game.
     * @return size of the square game
     */
    public int getSize() { return theSize; }

    /**
     * Change the number of rows/columns in this universe.
     * All cells are set to empty by this change.
     * 
     * @param newSize
     */
    public void setSize(int newSize)
    {
        theSize = newSize;
        cells = new Hashtable<Integer, Integer>();
    }


    /**
     * Get the state of the cell in a specified position. A non-zero value indicates that
     * the cell is live. The actual value is the age (number of generations it has been alive) of
     * that cell.
     *   
     * @param x  column of the desired cell
     * @param y  row of the desired cell
     * @return
     */
    public int getCell(int x, int y) 
    {
        Integer age = cells.get(key(x,y));
        if (age == null)
            return 0;
        else
            return age.intValue();
    }


    /**
     * Reset the current game to athe most recently used starting pattern
     */
    public void reset ()  {reset(pattern);}

    /**
     * Reset the game to a selected starting pattern.
     * @param patternNumber
     */
    public void reset (int patternNumber)
    {
    	pattern = patternNumber;
        cells = new Hashtable<Integer, Integer>();
        int m = theSize / 2;
        if (patternNumber > 0) {
        	String[] pattern = starters[patternNumber-1];
        	int h = pattern.length;
        	int w = pattern[0].length();
        	for (int y = 0; y < h; ++y)
        		for (int x = 0; x < w; ++x) {
        			if (pattern[y].charAt(x) == '*') {
        				cells.put(key(x + m - w/2,  y + m - h/2), 1);
        			}
        		}
        } else if (patternNumber < 0) {
        	reset(0.5);
        } else {
        	// Default (box) pattern
            for (int x = 0; x < theSize; ++x) {
                cells.put (key(x,0), new Integer(1));
                cells.put (key(x,theSize-1), new Integer(1));
                cells.put (key(0,x), new Integer(1));
                cells.put (key(theSize-1,x), new Integer(1));
                cells.put (key(x,x), new Integer(1));
                cells.put (key(x,theSize-1-x), new Integer(1));
            }
        }
    }



    public void reset (double density)
    {
    	pattern = -1;
        cells = new Hashtable<Integer, Integer>();
        for (int x = 0; x < theSize; ++x)
            for (int y = 0; y < theSize; ++y)
                if (Math.random() <= density) {
                    cells.put (key(x,y), new Integer(1));
                }
    }


    public void nextGeneration ()
    {
        Hashtable<Integer, Integer> newCells = new Hashtable<Integer, Integer>();
        for (int x = 0; x < theSize; ++x)
            for (int y = 0; y < theSize; ++y) {
                int oldValue = getCell(x,y);
                int sum = 0;
                for(int dx = -1; dx <= 1; ++dx)
                    for(int dy = -1; dy <= 1; ++dy)
                        if (dx != 0 || dy != 0)
                            if (getCell(x+dx,y+dy) != 0)
                                ++sum;
                if (oldValue == 0) {
                    if (sum == 3)
                        newCells.put(key(x,y), new Integer(1));
                } else {
                    if (sum == 2 || sum == 3)
                        newCells.put(key(x,y), new Integer(oldValue+1));
                }
            }
        cells = newCells;
    }


    private Integer key(int x, int y)
    {
        int k = -1;
        if (x >= 0 && y >= 0 && x < theSize && y < theSize) {
            k = x * theSize + y;
        }
        return new Integer(k);
    }

}
