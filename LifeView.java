import java.awt.*;
import javax.swing.*;

/**
 * This class provides a panel for viewing the Conway's game of life as
 * implemented in class Life. Colors are used to indicate the "age" of cells.
 * Red cells are new, shading through to blue for very old cells.
 */
public class LifeView extends JPanel {

	private Life theModel;
	private int generationCount;
	private int modelSize;

	private JLabel generationDisplay;
	private JPanel canvas;
	private JLabel sizeDisplay;

	private Color[] colorMap;

	private boolean canvasRepainted = false;

	private boolean stopping = false;
	private boolean pausing = true;

	/**
	 * This thread advances the generations of the Life game, drawing each
	 * generation.
	 *
	 */
	class Generator extends Thread {
		Generator() {
		}

		public void run() {
			while (!stopping) {
				
				try { sleep(50); } catch (InterruptedException e) { break; }
				 
				synchronized (this) {
					while (pausing) {
						try {
							wait();
						} catch (InterruptedException e) {
							return;
						}
					}
				}
				nextGeneration();
			}
		}
	}

	private Generator generator;

	/** Construct the GUI **/
	public LifeView() {
		modelSize = 128;

		theModel = new Life(modelSize);

		setLayout(new BorderLayout());
		JPanel displayPanel = new JPanel();
		generationDisplay = new JLabel("           ");
		sizeDisplay = new JLabel("" + modelSize);
		displayPanel.add(new JLabel("generation: "));
		displayPanel.add(generationDisplay);
		displayPanel.add(new JLabel("      size: "));
		displayPanel.add(sizeDisplay);
		setGenerationCount(0);

		canvas = new JPanel() {
			public void paint(Graphics g) {
				Image buffer = createImage(canvas.getSize().width,
						canvas.getSize().height);
				Graphics gbuffer = buffer.getGraphics();
				paintCanvas(gbuffer);
				g.drawImage(buffer, 0, 0, this);
				canvasRepainted = true;
			}

			public void update(Graphics g) {
				paint(g);
			}
		};
		canvas.setBackground(Color.black);

		add(displayPanel, BorderLayout.NORTH);
		add(canvas, BorderLayout.CENTER);

		colorMap = new Color[10];
		for (int i = 0; i < 10; ++i) {
			float hue = ((float) i) / (float) 10.0;
			colorMap[i] = Color.getHSBColor(hue, (float) 0.9, (float) 0.75);
		}
		generator = new Generator();
		generator.start();
	}

	/** How many cells for the width/height of the model? */
	public int getModelSize() {
		return modelSize;
	}

	/** Set the number of cells in the width & height of the model. */
	public void setModelSize(int newSize) {
		synchronized (theModel) {
			modelSize = newSize;
			theModel.setSize(modelSize);
			sizeDisplay.setText("" + modelSize);
			reset();
			theModel.notifyAll();
		}
	}

	/**
	 * Clear the current cells and replace with the indicated starting pattern.
	 */
	public void reset(int pattern) {
		synchronized (theModel) {
			theModel.reset(pattern);
			setGenerationCount(0);
			canvas.repaint();
			theModel.notifyAll();
		}
	}

	/**
	 * Clear the current cells and replace with the previous starting pattern.
	 */
	public void reset() {
		synchronized (theModel) {
			theModel.reset();
			setGenerationCount(0);
			canvas.repaint();
			theModel.notifyAll();
		}
	}

	/**
	 * Start or resume running the Life generations
	 */
	public void go() {
		synchronized (generator) {
			pausing = false;
			generator.notifyAll();
		}
	}

	/**
	 * Pause the Life simulation. (Important: this must be a true pause - no
	 * busy waits or sleeps.
	 */
	public void pause() {
		pausing = true;
	}

	/**
	 * Clear the current cells and replace with a random pattern
	 */
	public void reset(double density) {
		synchronized (theModel) {
			theModel.reset(density);
			setGenerationCount(0);
			canvas.repaint();
			theModel.notifyAll();
		}
	}

	/** Compute the next generation. */
	public void nextGeneration() {
		// System.err.println("Computing generation " + generationCount);
		synchronized (theModel) {
			theModel.nextGeneration();
			setGenerationCount(generationCount + 1);
			canvasRepainted = false;
			canvas.repaint();
			theModel.notifyAll();
		}
	}

	/** Set the current generation number and display it. */
	private void setGenerationCount(int c) {
			generationCount = c;
			generationDisplay.setText("" + generationCount);
	}

	/**
	 * The main function for displaying the Life game area.
	 */
	private synchronized void paintCanvas(Graphics g) {
		Dimension canvasSize = canvas.getSize();
		int xdelta = canvasSize.width / modelSize;
		if (xdelta == 0)
			xdelta = 1;
		int ydelta = canvasSize.height / modelSize;
		if (ydelta == 0)
			ydelta = 1;

		for (int x = 0; x < modelSize; ++x)
			for (int y = 0; y < modelSize; ++y) {
				int age = theModel.getCell(x, y);
				// System.out.print("("+x+","+y+"): " + age);
				if (generationCount >= 10 && age > 0)
					age = 1 + 10 * age / generationCount;
				if (age >= 10)
					age = 9;
				Color c = (age > 0) ? colorMap[age - 1] : Color.black;
				// System.out.println(" " + age + "  color: " + c);
				g.setColor(c);
				g.fillRect(x * xdelta, y * ydelta, xdelta, ydelta);
			}
	}

	public Dimension getPreferredSize() {
		return new Dimension(550, 550);
	}

}
