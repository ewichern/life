import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class LifeMain extends JFrame {

	private LifeView viewer;
	private Choice initialPic;
	private boolean inApplet;

	public LifeMain(boolean inAnApplet) {
		inApplet = inAnApplet;
		getContentPane().setLayout(new BorderLayout());

		JPanel controlPanel = new JPanel();

		getContentPane().add(controlPanel, BorderLayout.NORTH);

		initialPic = new Choice();
		initialPic.add("Random");
		for (String name : Life.patternNames) {
			initialPic.add(name);
		}
		initialPic.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					String value = initialPic.getSelectedItem();
					if (value.equals("Random"))
						viewer.reset(0.5);
					else {
						for (int i = 0; i < Life.patternNames.length; ++i) {
							if (value.equals(Life.patternNames[i])) {
								viewer.reset(i);
								break;
							}
						}
					}
				}
			}
		});
		controlPanel.add(initialPic);



		controlPanel.add(new JLabel("  Zoom: "));

		JButton zoomIn = new JButton("in");
		zoomIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int size = viewer.getModelSize();
				if (size > 1)
					size = size / 2;
				viewer.setModelSize(size);
				viewer.reset();
			}
		});
		controlPanel.add(zoomIn);

		JButton zoomOut = new JButton("out");
		zoomOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int size = viewer.getModelSize();
				viewer.setModelSize(2 * size);
				viewer.reset();
			}
		});
		controlPanel.add(zoomOut);

		viewer = new LifeView();
		getContentPane().add(viewer, BorderLayout.CENTER);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				if (inApplet)
					setVisible(false);
				else
					System.exit(0);
			}
		});
		
		JPanel bottomPanel = new JPanel();
		getContentPane().add(bottomPanel, BorderLayout.SOUTH);

		JButton go = new JButton("Go");
		go.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				viewer.go();
			}
		});
		bottomPanel.add(go);
		
		JButton pause = new JButton("Pause");
		pause.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				viewer.pause();
			}
		});
		bottomPanel.add(pause);
		
		JButton reset = new JButton("Reset");
		reset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				viewer.reset();
			}
		});
		bottomPanel.add(reset);
	}

	public static void main(String args[]) {
		LifeMain window = new LifeMain(false);
		window.setTitle("CS330 Asst - Life GUI");
		window.pack();
		window.setVisible(true);
	}

}
