#JAVAPATH=/research/java/jdk1.5.0_05/bin/
#CLASSPATH=.:/research/java/j2sdk1.4.2/lib/classes.zip
JOPTIONS=-g
USERHOME=$(HOME)

.SUFFIXES:
.SUFFIXES: .class .java
.java.class: $(JAVAPATH)javac $(JOPTIONS) $*.java

#
# As you define new classes, add them to the following list.
# It may not be absolutely necessary, but it will help guarantee that
# necessary recompilation gets done.
#
TOPPACKAGE=.
CLASSES:=$(patsubst %.java,%.class,$(wildcard *.java))

TARGET=LifeMain
ASSIGNMENT=life_f14

%.class: %.java
	$(JAVAPATH)javac  $(JOPTIONS) $*.java


all: $(CLASSES)

runsample:
	$(JAVAPATH)java  -cp /home/zeil/courses/cs330/Assignments/Life/$(ASSIGNMENT).jar LifeMain

jar: $(ASSIGNMENT).jar


$(ASSIGNMENT).jar: $(CLASSES)
	$(JAVAPATH)jar cfe $(ASSIGNMENT).jar LifeMain  *.class


retroguard: $(ASSIGNMENT).jar retroguard.dat
	mv $(ASSIGNMENT).jar temp.jar
	$(JAVAPATH)java -cp .:junit-4.11.jar:/home/zeil/src/retroguard/retroguard.jar RetroGuard temp.jar $(ASSIGNMENT).jar
	rm temp.jar 


clean:
	-rm -f *.class
	-rm -f $(ASSIGNMENT).jar



zip: 
	-rm -f assignment.zip
	zip -9 -r assignment.zip *.java makefile




